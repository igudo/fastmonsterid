from fastapi import APIRouter, Depends, HTTPException, status, Form
from fastapi.security import OAuth2PasswordBearer
from fastapi.responses import Response
from async_lru import alru_cache
from models import User
from tortoise.exceptions import DoesNotExist
import aiohttp
import jwt
import os

router = APIRouter()
JWT_SECRET = os.getenv('JWT_SECRET', "SECRET")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl='token')


async def authenticate_user(username: str, password: str):
    """Get user by username and password, returns None if not found"""
    try:
        user = await User.get(username=username)
    except DoesNotExist:
        return
    if not user.check_password(password):
        return False
    return user


@router.post('/token')
async def generate_token(username: str = Form(...), password: str = Form(...)):
    """Custom function for token generating"""
    user = await authenticate_user(username, password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Invalid username or password'
        )
    payload = {"username": user.username, "id": user.id}
    token = jwt.encode(payload, JWT_SECRET)
    return {'access_token': token, 'token_type': 'bearer'}


async def get_current_user(token: str = Depends(oauth2_scheme)):
    """Auth-check wrapper, returns user credentials"""
    try:
        payload = jwt.decode(token, JWT_SECRET, algorithms=['HS256'])
        user = await User.get(id=payload["id"])
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Unauthorized'
        )
    return user


@alru_cache(maxsize=None)
async def generate_emoticon(text):
    """Returns image by input text"""
    async with aiohttp.ClientSession() as session:
        async with session.get("http://monsterid:"+os.getenv('MONSTERID_PORT', "8080")+"/monster/"+text) as response:
            return Response(content=await response.content.read(), media_type="image/png")


@router.post('/generate')
async def generate_emoticon_wrapper(user=Depends(get_current_user), text: str = Form(...)):
    """Wrapper on generate_emoticon for user dependency"""
    return await generate_emoticon(text)


@router.on_event("startup")
async def startup_event():
    if int(os.getenv("CREATE_TEST_USER", "0")):
        if len(await User.all()):
            return
        test_user = await User.create(username="test")
        test_user.password = test_user.hash_password("test")
        await test_user.save()
