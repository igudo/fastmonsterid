from fastapi import FastAPI
from tortoise.contrib.starlette import register_tortoise
import os


def init(app: FastAPI):
    """
    Init app routers
    """
    init_db(app)
    from routers import router
    app.include_router(router)


def init_db(app: FastAPI):
    """
    Init database models
    """
    register_tortoise(
        app,
        config={
            'connections': {
                'default': {
                    'engine': 'tortoise.backends.asyncpg',
                    'credentials': {
                        'host': os.environ['POSTGRES_HOST'],
                        'port': os.environ['POSTGRES_PORT'],
                        'user': os.environ['POSTGRES_USER'],
                        'password': os.environ['POSTGRES_PASSWORD'],
                        'database': os.environ['POSTGRES_DB'],
                    }
                },
            },
            'apps': {
                'models': {
                    'models': ['models'],
                }
            }
        },
        generate_schemas=True
    )
