from fastapi.testclient import TestClient
from tortoise.contrib.test import finalizer, initializer
from main import app
import asyncio
import pytest

client = TestClient(app)


def test_generate_token_no_data(client: TestClient, event_loop: asyncio.AbstractEventLoop):
    response = client.post("/token")
    assert response.status_code == 422


def test_generate_token_wrong(client: TestClient, event_loop: asyncio.AbstractEventLoop):
    response = client.post("/token", data={"username": "test", "password": "wrong"})
    assert response.status_code == 401
    assert response.json() == {"detail": "Invalid username or password"}


def test_generate_token_success(client: TestClient, event_loop: asyncio.AbstractEventLoop):
    response = client.post("/token", data={"username": "test", "password": "test"})
    assert response.status_code == 200
    assert "access_token" in response.json()


def test_generate_emoticon(client: TestClient, event_loop: asyncio.AbstractEventLoop):
    token = client.post("/token", data={"username": "test", "password": "test"}).json()["access_token"]
    response = client.post("/generate", data={"text": "123"}, headers={"Authorization": "Bearer "+token})
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "image/png"


def test_generate_emoticon_same(client: TestClient, event_loop: asyncio.AbstractEventLoop):
    token = client.post("/token", data={"username": "test", "password": "test"}).json()["access_token"]
    response1 = client.post("/generate", data={"text": "321"}, headers={"Authorization": "Bearer "+token})
    response2 = client.post("/generate", data={"text": "321"}, headers={"Authorization": "Bearer "+token})
    assert response1.status_code == 200
    assert response2.status_code == 200
    assert response1.content == response2.content


def test_generate_emoticon_no_token(client: TestClient, event_loop: asyncio.AbstractEventLoop):
    response = client.post("/generate", data={"text": "123"})
    assert response.status_code == 401


def test_generate_emoticon_wrong_token(client: TestClient, event_loop: asyncio.AbstractEventLoop):
    response = client.post("/generate", data={"text": "123"}, headers={"Authorization": "Bearer wrong"})
    assert response.status_code == 401


@pytest.fixture(scope="module")
def client():
    # Link with DB for testing
    initializer(
        ["models"]
    )

    with TestClient(app) as test_client:
        # testing
        yield test_client

    # tear down
    finalizer()


@pytest.fixture(scope="module")
def event_loop(client: TestClient):
    yield client.task.get_loop()
