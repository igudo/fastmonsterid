from fastapi import FastAPI
from initializer import init

app = FastAPI(title="MonsterID API")

init(app)
