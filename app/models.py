from tortoise import fields, models
from passlib.context import CryptContext


class User(models.Model):
    """
    User model
    """

    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=30, unique=True)
    password = fields.TextField(null=True)

    # password hashing
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    def check_password(self, other_password):
        return self.pwd_context.verify(other_password, self.password)

    def hash_password(self, password):
        return self.pwd_context.hash(password)

