# MonsterID API
REST API service for monsterID

## Requirements
<div class="termy">

```console
docker
docker-compose
```
</div>


## Run it
<div class="termy">

```console
$ docker-compose up -d
```
</div>


## Run tests
<div class="termy">

```console
$ docker-compose -f docker-compose.test.yml up 
```
</div>

